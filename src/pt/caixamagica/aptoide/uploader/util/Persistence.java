/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util;

import java.util.HashMap;

import pt.caixamagica.aptoide.uploader.dataviews.AbstractViewDevelopper;
import pt.caixamagica.aptoide.uploader.dataviews.NullViewDevelopper;
import pt.caixamagica.aptoide.uploader.dataviews.ViewDevelopper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author dsilveira
 * 
 * adapted from notepad tutorial
 *
 */
public class Persistence {

    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_REPOSITORY = "repository";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_BATCH_UPLOADER = "batch_uploader";
    public static final String KEY_DEFAULT = "default_login";
    public static final String KEY_ROWID = "_id";

    private static final String TAG = "AptoideUploader-Persistence";
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
        "create table data ("
    	+ "_id integer primary key autoincrement, "
        + "username text not null, "
        + "password text not null, "
        + "repository text not null, "
        + "token text not null, "
        + "batch_uploader integer not null, "
        + "default_login integer not null "
        + ");";

    private static final String DATABASE_NAME = "persistence";
    private static final String DATABASE_TABLE = "data";
    private static final int DATABASE_VERSION = 3;

    private final Context context;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "Upgrading database from version " + oldVersion + " to "+ newVersion );

        	AbstractViewDevelopper developper = new NullViewDevelopper();
			
            for(int version=oldVersion; version <= newVersion; version++){
            	switch (version) {
				case 2:
					HashMap<String,String> login = new HashMap<String, String>();
					
					
			    	Cursor cursor = db.query(DATABASE_TABLE, null, KEY_DEFAULT+"=?", new String[]{"1"}, null, null, null);
			    	cursor.moveToFirst();
			    	for (int columnIndex=1; columnIndex < cursor.getColumnCount(); columnIndex++){
						login.put(cursor.getColumnName(columnIndex), cursor.getString(columnIndex));
			    	}
			    	
			    	developper = new ViewDevelopper(login.get(KEY_USERNAME), login.get(KEY_PASSWORD));
			    	developper.setRepository(login.get(KEY_REPOSITORY));
			    	developper.setToken(login.get(KEY_TOKEN));
			    	
			    	cursor.close();
			    	Log.d(TAG, "fetched default login: DBversion-"+version+" user-"+developper.getUsername());
					break;
					
				case 3:
			    	developper.setBatchUploader( false );
			    	Log.d(TAG, "fetched set login: DBversion-"+version+" user-"+developper.getUsername()+" isBatchUploader-"+false);
					break;

				default:
					break;
				}
            }
            db.execSQL("DROP TABLE IF EXISTS data");
            onCreate(db);

            
            ContentValues initialValues = new ContentValues();
            
            initialValues.put(KEY_USERNAME, developper.getUsername());
       		initialValues.put(KEY_PASSWORD, developper.getPassword());
       		initialValues.put(KEY_REPOSITORY, developper.getRepository());
       		initialValues.put(KEY_TOKEN, developper.getToken());
       		initialValues.put(KEY_BATCH_UPLOADER, (developper.isBatchUploader()?"1":"0"));
       		initialValues.put(KEY_DEFAULT, "1");
       		
       		db.insert(DATABASE_TABLE, null, initialValues);
       		initialValues.clear();
        	Log.d("AptoideUploader-Persistence", "reStored default login: "+developper.toString());

            return;
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public Persistence(Context ctx) {
        this.context = ctx;
    }

    /**
     * Open the database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public Persistence open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }


    /**
     * Stores login data
     * 
     * @param developper view
     * @return void
     */
    public void storeLoginData(ViewDevelopper developper) {
        ContentValues initialValues = new ContentValues();
        
        initialValues.put(KEY_USERNAME, developper.getUsername());
   		initialValues.put(KEY_PASSWORD, developper.getPassword());
   		initialValues.put(KEY_REPOSITORY, developper.getRepository());
   		initialValues.put(KEY_TOKEN, developper.getToken());
   		initialValues.put(KEY_BATCH_UPLOADER, (developper.isBatchUploader()?"1":"0"));
   		initialValues.put(KEY_DEFAULT, "0");

   		long inserted = db.insert(DATABASE_TABLE, null, initialValues);
        initialValues.clear();
        if ( inserted == -1){
        	Log.e("AptoideUploader-Persistence", "developper not inserted");
        }
        Log.d("AptoideUploader-Persistence", "stored "+developper.toString());
        return;
    }
    
    
    /**
     * Stores default login data
     * 
     * @param developper view
     * @return void
     */
    public void storeDefaultLoginData(AbstractViewDevelopper developper) {
   		
    	ContentValues noDefault = new ContentValues();
        ContentValues initialValues = new ContentValues();

    	noDefault.put(KEY_DEFAULT, "0");
        int updated = db.update(DATABASE_TABLE, noDefault, null, null);
        Log.d("AptoideUploader-Persistence", updated+" login un-defaulted");
        
        initialValues.put(KEY_USERNAME, developper.getUsername());
   		initialValues.put(KEY_PASSWORD, developper.getPassword());
   		initialValues.put(KEY_REPOSITORY, developper.getRepository());
   		initialValues.put(KEY_TOKEN, developper.getToken());
   		initialValues.put(KEY_BATCH_UPLOADER, (developper.isBatchUploader()?"1":"0"));
   		initialValues.put(KEY_DEFAULT, "1");

//   		Cursor cursor = db.rawQuery("select * from "+DATABASE_TABLE+" where username='"+developper.getUsername()+"'", null);
    	Cursor cursor = db.query(DATABASE_TABLE, null, KEY_USERNAME+"=?", new String[]{developper.getUsername()}, null, null, null);
   		if( cursor.getCount() > 0 ){
//   			db.rawQuery("delete from "+DATABASE_TABLE+" where username='"+developper.getUsername()+"'", null);
   			int deleted = db.delete(DATABASE_TABLE, KEY_USERNAME+"=?", new String[]{developper.getUsername()});
   			Log.d("AptoideUploader-Persistence", "deleted "+deleted+" "+developper.getUsername()+" username");
   		}
   		
   		db.insert(DATABASE_TABLE, null, initialValues);
   		initialValues.clear();
   		cursor.close();
    	Log.d("AptoideUploader-Persistence", "stored default login: "+developper.toString());

        return;
    }
    
    
    /**
     * Return a ViewDevelopper of this username
     * 
     * @param string username 
     * @return ViewDevelopper
     */
    public ViewDevelopper fetchLogin(String username) {
    	HashMap<String,String> login = new HashMap<String, String>();
    	ViewDevelopper developper;
    	
//    	Cursor cursor = db.rawQuery("select * from "+DATABASE_TABLE+" where "+KEY_USERNAME+"='"+username+"'", null);
    	Cursor cursor = db.query(DATABASE_TABLE, null, KEY_USERNAME+"=?", new String[]{username}, null, null, null);
    	cursor.moveToFirst();
    	for (int columnIndex=0; columnIndex < cursor.getColumnCount(); columnIndex++){
			login.put(cursor.getColumnName(columnIndex), cursor.getString(columnIndex));
    	}
    	developper = new ViewDevelopper(login.get(KEY_USERNAME), login.get(KEY_PASSWORD));
    	developper.setRepository(login.get(KEY_REPOSITORY));
    	developper.setToken(login.get(KEY_TOKEN));
    	developper.setBatchUploader( (login.get(KEY_BATCH_UPLOADER).equals("1")? true: false ));
    	cursor.close();
    	Log.d("AptoideUploader-Persistence", "fetched login: "+developper.toString());
        return developper;
    }
    
    
    /**
     * Return a ViewDevelopper of the default login data in the database
     * 
     * @return ViewDevelopper of login data
     */
    public AbstractViewDevelopper fetchDefaultLogin() {
    	HashMap<String,String> login = new HashMap<String, String>();
    	AbstractViewDevelopper developper = new NullViewDevelopper();
    	
//    	Cursor cursor = db.rawQuery("select * from "+DATABASE_TABLE+" where "+KEY_DEFAULT+"='1'", null);
    	Cursor cursor = db.query(DATABASE_TABLE, null, KEY_DEFAULT+"=?", new String[]{"1"}, null, null, null);
    	cursor.moveToFirst();
    	for (int columnIndex=1; columnIndex < cursor.getColumnCount(); columnIndex++){
			login.put(cursor.getColumnName(columnIndex), cursor.getString(columnIndex));
    	}
    	
    	try{
	    	developper = new ViewDevelopper(login.get(KEY_USERNAME), login.get(KEY_PASSWORD));
	    	developper.setRepository(login.get(KEY_REPOSITORY));
	    	developper.setToken(login.get(KEY_TOKEN));
	    	developper.setBatchUploader( (login.get(KEY_BATCH_UPLOADER).equals("1")? true: false ));
	    	
	    	cursor.close();

	    	Log.d("AptoideUploader-Persistence", "fetched default login: "+developper.toString());
	        return developper;
	        
    	}catch (NullPointerException e) {
			developper = fetchDefaultLogin();
		}
		return developper;
    }
    
    
    /**
     * Return true if a default login exists
     * 
     * @return boolean
     */
    public Boolean defaultExists() {
//    	Cursor cursor = db.rawQuery("select * from "+DATABASE_TABLE+" where "+KEY_DEFAULT+"='1'", null);
    	Cursor cursor = db.query(DATABASE_TABLE, new String[]{KEY_USERNAME}, KEY_DEFAULT+"=?", new String[]{"1"}, null, null, null);
    	
    	if( cursor.getCount() > 0 ){
    		cursor.close();
    		Log.d("AptoideUploader-Persistence", "default exists");
    		return true;
    	}else{
    		cursor.close();
    		Log.d("AptoideUploader-Persistence", "no default exists");
    		return false;
    	}
    }

    
    /**
     * Cleanup method
     */    
    public void clear() {
    	if(db.isOpen()){
    		db.execSQL("drop table "+DATABASE_TABLE);
    		Log.d("AptoideUploader-Persistence", DATABASE_TABLE+"table dropped");
    	}
    }
    
}