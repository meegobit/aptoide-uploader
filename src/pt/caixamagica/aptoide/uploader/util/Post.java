/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import pt.caixamagica.aptoide.uploader.dataviews.AbstractViewDevelopper;
import pt.caixamagica.aptoide.uploader.dataviews.ViewApk;
import pt.caixamagica.aptoide.uploader.util.cookies.AbstractCookie;
import pt.caixamagica.aptoide.uploader.util.cookies.Cookie;
import pt.caixamagica.aptoide.uploader.util.cookies.NullCookie;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.IdUndetectedBySoupException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullTokenRetrievalException;
import android.util.Log;


public class Post {
	
	private static final String USER_AGENT = "Mozilla/5.0 (X11; Arch Linux i686; rv:2.0) Gecko/20110321 Firefox/4.0";
	private static final String LINE_END = "\r\n";
	private static final String TWO_HYPHENS = "--";
	private static final int CHUNK_SIZE = 8096;
	
	private String endpoint;
	
	private AbstractCookie cookie;
	
	private String boundary;
	
	private String body;
	private boolean submitting;
	private String apkPath;
	private String apkName;
	
	private HttpURLConnection connection = null;
	HttpResponse httpResponse;
	
	private String response;
	private boolean hasJsonResponse;
	private String errors;

	public Post(String endpoint) {
		this.endpoint = endpoint;
		this.boundary = generateBoundary();
		this.body = "";
		this.submitting = false;
		this.cookie = new NullCookie();
		this.hasJsonResponse = false;
	}
	
	
	public void setJsonResponseMode(){
		this.hasJsonResponse = true;
	}
	
	protected String getEndpoint() {
		return endpoint;
	}

	protected void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	
	public void retrieveCookie(){
		Map<String, List<String>> headers = connection.getHeaderFields();
		for (Map.Entry<String, List<String>> header : headers.entrySet()) {
			if(header.getKey().equalsIgnoreCase("Set-Cookie")){
				cookie = new Cookie( header.getValue().get(0) );
				Log.d("AptoideUploader-Post", "Cookie:" + cookie);
			}
		}
	}


	public AbstractCookie getCookie() {
		if(cookie.isNull()){
			retrieveCookie();
		}
		return cookie;
	}


	public void setCookie(AbstractCookie cookie) {
		this.cookie = cookie;
	}



	protected String getBoundary() {
		return boundary;
	}
	

	protected String getBody() {
		return body;
	}



	protected void setBody(String body) {
		this.body = body;
	}



	public static String generateBoundary() { 
		try {
			// Create a secure random number generator
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");

			// Get 1024 random bits
			byte[] bytes = new byte[1024/8];
			sr.nextBytes(bytes);
			
			int seedByteCount = 10;
			byte[] seed = sr.generateSeed(seedByteCount);

			sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed(seed);
			
			return "***"+Long.toString(sr.nextLong())+"***";
			
		} catch (NoSuchAlgorithmException e) {
		}
		return "*********";
	}
	
	private String formPart(String fieldName, String fieldValue){
		return   TWO_HYPHENS + boundary + LINE_END
			   + "Content-Disposition: form-data; name=\"" + fieldName + "\""
			   + LINE_END + LINE_END + fieldValue + LINE_END;
	}
	
	private String formBinaryPartNoTail(String fieldName, String contentType){
		return  TWO_HYPHENS + boundary + LINE_END
			  +	"Content-Disposition: form-data; name=\"" + fieldName + "\"; "
			  + "filename=\"" + apkName + ".apk\"" + LINE_END
			  + "Content-Type: " + contentType + LINE_END + LINE_END;  
	}
	
	
	
	public void buildScrappingLoginBody(AbstractViewDevelopper developper, String sendTo){
		body = formPart("username", developper.getUsername() )
			 + formPart("password", developper.getPassword() )
			 + formPart("sendTo", sendTo)
			 + formPart("login", "Login")
			 + getMultipartPostFooter();
	}
	
	public void buildLoginBody(AbstractViewDevelopper developper){
		
		
		String password = null;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA");
	        md.update(developper.getPassword().getBytes());

	        password = Security.byteArrayToHexString(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		Log.d("AptoideUploader-Post", "passwordHash: "+password);
		
		body = formPart("user", developper.getUsername() )
			 + formPart("passhash", password )
			 + formPart("mode", "json" )
			 + getMultipartPostFooter();					 
	}
	
	public void buildSubmitBodyNoFooter(AbstractViewDevelopper developper, ViewApk apk){
		apkPath = apk.getPath();
		apkName = apk.getName();
		
		body = formPart("token", developper.getToken() );
		body += formPart("isAptUploader", "true");
		
		if(developper.getPhone() != null){
			body += formPart("apk_phone", developper.getPhone() );
		}
		if(developper.getEmail() != null){
			body += formPart("apk_email", developper.getEmail() );
		}
		if(developper.getWebURL() != null){
			body += formPart("apk_website", developper.getWebURL() );
		}
		if(apk.getCategory() != null){
			body += formPart("category", apk.getCategory() );
		}
		
		body += formPart("repo", developper.getRepository() )
			 + formPart("apkname", apk.getName() )
			 + formPart("description", apk.getDescription() )
			 + formPart("rating", apk.getRating() )
			 + formPart("mode", "json" )
			 + formBinaryPartNoTail("apk", "application/vnd.android.package-archive");
		
		submitting = true;
			 
	}
	
	public int outputApk(DataOutputStream outputStream) throws FileNotFoundException, IOException{
		FileInputStream apk = new FileInputStream(apkPath);
		byte data[] = new byte[CHUNK_SIZE];
		Integer lenTotal = 0;
		int read;
		while((read = apk.read(data, 0, CHUNK_SIZE)) != -1) {
            outputStream.write(data,0,read);
            lenTotal += read;
            Log.d("OutputApk", "sent: "+read+"bytes, Total: "+lenTotal);
		}
		return lenTotal;
		
	}
	
	public String getMultipartPostFooter(){
		return LINE_END
			 + TWO_HYPHENS + boundary + TWO_HYPHENS + LINE_END;
	}
	
	public int multipartOutput() throws MalformedURLException, ProtocolException, IOException{
		DataOutputStream outputStream = null;

			URL url = new URL(endpoint);
			connection = (HttpURLConnection) url.openConnection();
			
			// This fixes #515 : Out of memory bug
			connection.setChunkedStreamingMode(CHUNK_SIZE);

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");
			
			connection.setInstanceFollowRedirects(true);

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
			connection.setRequestProperty("User-Agent", USER_AGENT);
			
			if(!cookie.isNull()){
				connection.addRequestProperty("Cookie", cookie.getCookie());
			}

			outputStream = new DataOutputStream( connection.getOutputStream() );
	
			outputStream.writeBytes(body);
			
			if(submitting){
				outputApk(outputStream);
				
				outputStream.writeBytes(getMultipartPostFooter());
			}

			outputStream.flush();
			outputStream.close();
			
			getMultipartResponseBody();
			
			return getMultipartResponseCode();
		
	}
	
	public int textOutput() throws ClientProtocolException, IOException{
		HttpClient httpClient = new DefaultHttpClient();
		 
        // Prepare a request object
        HttpPost httpPost = new HttpPost(endpoint); 
        
        httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        httpPost.setHeader("Content-Type", "text/html");
        httpPost.setHeader("User-Agent", USER_AGENT);
        if(!cookie.isNull()){
        	httpPost.setHeader("Cookie", cookie.getCookie());
        }
        
        httpPost.setEntity(null);
        
    	httpResponse = httpClient.execute(httpPost);

    	getTextResponseBody();
    	
    	return getTextResponseCode();
    	
	}
	
	public int getMultipartResponseCode() throws IOException{
		return connection.getResponseCode();
	}
	
	public int getTextResponseCode() throws IOException{
		StatusLine statusLine = httpResponse.getStatusLine();
    	
    	return statusLine.getStatusCode();
	}
	
	public String getMultipartResponseMessage() throws IOException{
		return connection.getResponseMessage();
	}
	
	public String getTextResponseMode() throws IOException{
		StatusLine statusLine = httpResponse.getStatusLine();
    	
    	return statusLine.getReasonPhrase();
	}
	
	
	public String getMultipartResponseBody() throws IOException{
		InputStream inputStream = connection.getInputStream();
		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, "UTF-8");
		response = writer.toString();
		inputStream.close();

		return response;
	}	
	
	public String getTextResponseBody() throws ParseException, IOException{
        
    	if (httpResponse != null) {
    		response = EntityUtils.toString(httpResponse.getEntity());
//        	Log.d("AptoidUploader", tokenPage);
    	}

    	return response;
	}
	
	public boolean hasErrors() throws JSONException{
		if(hasJsonResponse){
			JSONObject json;
			String status;
			
			json = new JSONObject(response);
			status = json.getString("status");
			if(status.equals("OK")){
				Log.d("AptoideUploader-Post-Errors", json.getString("errors")); //webservice returns success messages under errors JSON tag
			}else{
				errors = json.getString("errors");
			}
			
		}else{
			Document dom = Jsoup.parse(response);
			Element error = dom.getElementById("errors");
			if( !(error == null) ){
				errors = error.text();
			}
		}
		if(errors == null){
			Log.d("AptoideUploader-Post-Errors", "Status OK");
			return false;
		}
		Log.d("AptoideUploader-Post-Errors", errors);
		return true;
	}
	
	public String getErrors(){
		return errors;
	}
	
	
	
	
	public String getToken() throws JSONException, IdUndetectedBySoupException, UnsuccessfullTokenRetrievalException {
		if(hasJsonResponse){
			JSONObject json;
			String status;
			
			json = new JSONObject(response);
			status = json.getString("status");
			if(status.equals("OK")){
				return json.getString("token");
			}else{
				errors = json.getString("errors");
				throw new UnsuccessfullTokenRetrievalException("JSON Status not OK");
			}
		}else{
    		Document dom = Jsoup.parse(response);
    		Element token = dom.getElementById("token");
    		if(token==null){
    			errors = "No token";
    			throw new IdUndetectedBySoupException();
    		}
    		Log.d("AptoideUploader-Post", "Token: "+ token.val());
		        
		    return token.val();
		}
	}
}
