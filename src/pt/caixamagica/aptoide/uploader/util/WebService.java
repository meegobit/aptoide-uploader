/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util;

import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.ProtocolException;
//import org.json.JSONException;

import pt.caixamagica.aptoide.uploader.dataviews.AbstractViewDevelopper;
import pt.caixamagica.aptoide.uploader.dataviews.ViewApk;
import pt.caixamagica.aptoide.uploader.util.cookies.AbstractCookie;
import pt.caixamagica.aptoide.uploader.util.webstates.AbstractWebState;
import pt.caixamagica.aptoide.uploader.util.webstates.Error;
import pt.caixamagica.aptoide.uploader.util.webstates.LoggingIn;
import pt.caixamagica.aptoide.uploader.util.webstates.Submitting;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullPostException;
import android.util.Log;


/**
 * @author dsilveira
 *
 */
public class WebService {
	
	private AbstractWebState webState;
	private AbstractViewDevelopper developper;
	private ViewApk apk;
	private AbstractCookie cookie;
	private EnumErrorStates errorName;
	private int retrys = 0;
	
	
	public AbstractCookie getCookie() {
		return cookie;
	}

	public void setCookie(AbstractCookie cookie) {
		this.cookie = cookie;
	}
	
	public AbstractViewDevelopper getDevelopper() {
		return developper;
	}

	public void setDevelopper(AbstractViewDevelopper developper2) {
		this.developper = developper2;
	}

	public ViewApk getApk() {
		return apk;
	}

	public void setApk(ViewApk apk) {
		this.apk = apk;
	}

	public EnumWebStateName getStateName() {
		return webState.getStateName();
	}

	public AbstractWebState getWebState() {
		return webState;
	}

	public void setWebState(AbstractWebState webState) {
		this.webState = webState;
	}

	public int getResponseCode(){
		return webState.getResponseCode();
	}
	
	public String getToken() {
		return webState.getToken();
	}
	
	public String getError(){
		return webState.getError();
	}	
	
	public EnumErrorStates getErrorName(){
		return errorName;
	}
	
	public void setErrorName(EnumErrorStates errorName){
		this.errorName = errorName;
	}

	
	public String login(AbstractViewDevelopper developper){
		setDevelopper(developper);
		setErrorName(EnumErrorStates.NO_ERROR);
		
		try {
			setWebState( new LoggingIn(this) );
			webState.post();
			
			return getToken();
			
		} catch (UnsuccessfullPostException e) {
			setWebState( new Error(getError()) );
			e.printStackTrace();
		} 
//		catch (JSONException e) {
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (ProtocolException e) {
//			e.printStackTrace();
//		} 
		catch (IOException e) {
			if(retrys < 3){
				retrys += 1;
				Log.d("AptoideUploader-WebService", "Connection failed, retrying... "+retrys);
				e.printStackTrace();
				login(developper);
			}else{
				Log.d("AptoideUploader-WebService", "Connection failed, giving up!");
				retrys = 0;
				e.printStackTrace();
				webState.setError("To login in Bazaar, you need a network connection. Please, check your network settings.");
				setErrorName(EnumErrorStates.CONNECTION_ERROR);
				setWebState( new Error(getError()) );
			}
		} catch (Exception e) {
			Log.d("AptoideUploader-WebService", "unpredictable error, giving up!");
			retrys = 0;
			e.printStackTrace();
			webState.setError("We're sorry, but an unpredictable error has occurred, you may try again.");
			setErrorName(EnumErrorStates.UNPREDICTABLE_ERROR);
			setWebState( new Error(getError()) );
		}
		
		return null;
	}
	
	public void submit(AbstractViewDevelopper developper, ViewApk apk){
		setDevelopper(developper);
		setApk(apk);
		setErrorName(EnumErrorStates.NO_ERROR);
		
		try {
			
			setWebState( new Submitting(this) );
			webState.post();
			
		} catch (UnsuccessfullPostException e) {
			if(getError().equals(EnumErrorStates.CONNECTION_ERROR)){
				if(retrys < 3){
					retrys += 1;
					Log.d("AptoideUploader-WebService", "Connection failed, retrying... "+retrys);
					e.printStackTrace();
					submit(developper, apk);
				}else{
					Log.d("AptoideUploader-WebService", "Connection failed, giving up!");
					retrys = 0;
					setWebState( new Error(getError()) );
					e.printStackTrace();
				}
			}else{
				setWebState( new Error(getError()) );
				e.printStackTrace();
			}
		} 
//		catch (JSONException e) {
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (ProtocolException e) {
//			e.printStackTrace();
//		} 
		catch (IOException e) {
			if(retrys < 3){
				retrys += 1;
				Log.d("AptoideUploader-WebService", "Connection failed, retrying... "+retrys);
				e.printStackTrace();
				submit(developper, apk);
			}else{
				Log.d("AptoideUploader-WebService", "Connection failed, giving up!");
				retrys = 0;
				e.printStackTrace();
				webState.setError("To submit to Bazaar, you need a network connection. Please, check your network settings.");
				setErrorName(EnumErrorStates.CONNECTION_ERROR);
				setWebState( new Error(getError()) );
			}
		} catch (Exception e) {
			Log.d("AptoideUploader-WebService", "unpredictable error, giving up!");
			retrys = 0;
			e.printStackTrace();
			webState.setError("We're sorry, but an unpredictable error has occurred, you may try again.");
			setErrorName(EnumErrorStates.UNPREDICTABLE_ERROR);
			setWebState( new Error(getError()) );
		}
		
		return;
	}
    
}
