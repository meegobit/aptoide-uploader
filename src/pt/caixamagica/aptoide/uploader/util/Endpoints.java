/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util;

public class Endpoints {

	private static final String LOGIN = "http://www.bazaarandroid.com/account/login";
	private static final String TOKEN = "http://www.bazaarandroid.com/webservices";
	private static final String LOGIN_WS = "http://www.bazaarandroid.com/webservices/checkUserCredentials";
	private static final String UPLOAD_WS = "http://www.bazaarandroid.com/webservices/uploadAppToRepo";
	
	
	public static String getLogin() {
		return LOGIN;
	}
	
	public static String getToken() {
		return TOKEN;
	}
	
	public static String getLoginWS() {
		return LOGIN_WS;
	}
	
	public static String getUploadWS() {
		return UPLOAD_WS;
	}
	
}
