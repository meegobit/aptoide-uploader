package pt.caixamagica.aptoide.uploader.util;

/**
 * @author rafael
 * 
 */
public class Security {

	private Security(){}
	
	/**
	 * Useful to convert the digest to the hash
	 * 
	 * @param byteArray
	 * @return
	 */
	public static String byteArrayToHexString(byte[] byteArray) {
		  StringBuilder result = new StringBuilder("");
		  for (int i=0; i < byteArray.length; i++) {
		    result.append(Integer.toString( (byteArray[i] & 0xff) + 0x100 , 16).substring( 1 ));
		  }
		  return result.toString();
	}

}
