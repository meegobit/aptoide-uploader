/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util.webstates;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.json.JSONException;

import android.util.Log;

import pt.caixamagica.aptoide.uploader.util.Endpoints;
import pt.caixamagica.aptoide.uploader.util.EnumErrorStates;
import pt.caixamagica.aptoide.uploader.util.EnumWebStateName;
import pt.caixamagica.aptoide.uploader.util.Post;
import pt.caixamagica.aptoide.uploader.util.WebService;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.IdUndetectedBySoupException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullPostException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullTokenRetrievalException;

/**
 * @author dsilveira
 *
 */
public class LoggingIn extends AbstractWebState {

	private static final String ENDPOINT = Endpoints.getLoginWS();

		
	public LoggingIn(WebService webService) {
		setWebService(webService);
		setPost( new Post(ENDPOINT) );
		getPost().setJsonResponseMode();
		setStateName( EnumWebStateName.LOGGING_IN );
		Log.d("AptoideUploader-WebState", getStateName().toString());
	}
	
	@Override
	public void post() throws MalformedURLException, ProtocolException, IOException, UnsuccessfullPostException, JSONException {
		getPost().buildLoginBody( getDevelopper());
		setResponseCode( getPost().multipartOutput() );
		
		if(getResponseCode() != 200){
			setError( getPost().getMultipartResponseMessage() );
			getWebService().setErrorName(EnumErrorStates.HTTP_SEQUENCE_ERROR);
			throw new UnsuccessfullTokenRetrievalException( getError() );
		}else{
			try{
				rememberToken();
				getWebService().setWebState(new LoggedIn(getWebService()));
			}catch (IdUndetectedBySoupException e) {
				// Only usefull when scrapping
			}catch (UnsuccessfullTokenRetrievalException e) {
//				getPost().hasErrors();
				setError( getPost().getErrors() );
				getWebService().setErrorName(EnumErrorStates.BAD_LOGIN);
				throw new UnsuccessfullTokenRetrievalException( getError() );
			}
		}
		return;
	}

}
