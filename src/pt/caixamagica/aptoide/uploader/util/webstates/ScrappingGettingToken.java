/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util.webstates;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.json.JSONException;

import pt.caixamagica.aptoide.uploader.util.Endpoints;
import pt.caixamagica.aptoide.uploader.util.EnumErrorStates;
import pt.caixamagica.aptoide.uploader.util.EnumWebStateName;
import pt.caixamagica.aptoide.uploader.util.Post;
import pt.caixamagica.aptoide.uploader.util.WebService;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.IdUndetectedBySoupException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullTokenRetrievalException;
import android.util.Log;

/**
 * @author dsilveira
 *
 */
public class ScrappingGettingToken extends AbstractWebState {

	private static final String ENDPOINT = Endpoints.getToken();

	public ScrappingGettingToken(WebService webService) {
		setWebService(webService);
		setPost( new Post(ENDPOINT) );
		getPost().setCookie(webService.getCookie());
		setStateName( EnumWebStateName.GETING_TOKEN );
		Log.d("AptoideUploader-WebState", getStateName().toString());
	}

	@Override
	public void post() throws MalformedURLException, ProtocolException, IOException, UnsuccessfullTokenRetrievalException, JSONException {
		setResponseCode( getPost().textOutput() );
		
		if(getResponseCode() != 200){
			getPost().hasErrors();
			setError( getPost().getErrors() );
			getWebService().setErrorName(EnumErrorStates.HTTP_SEQUENCE_ERROR);
			throw new  UnsuccessfullTokenRetrievalException( getError() );
		}else{
			try{
				rememberToken();
				getWebService().setWebState(new ScrappingLoggedIn(getWebService()));
			}catch (IdUndetectedBySoupException e) {
				getPost().hasErrors();
				setError( getPost().getErrors() );
				getWebService().setErrorName(EnumErrorStates.BAD_LOGIN);
				throw new UnsuccessfullTokenRetrievalException( getError() );
			}			
		}
		return;
	}

}
