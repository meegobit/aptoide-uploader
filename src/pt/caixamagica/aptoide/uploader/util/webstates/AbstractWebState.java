/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util.webstates;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.json.JSONException;

import pt.caixamagica.aptoide.uploader.dataviews.AbstractViewDevelopper;
import pt.caixamagica.aptoide.uploader.dataviews.ViewApk;
import pt.caixamagica.aptoide.uploader.util.EnumWebStateName;
import pt.caixamagica.aptoide.uploader.util.Post;
import pt.caixamagica.aptoide.uploader.util.WebService;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.IdUndetectedBySoupException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullPostException;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullTokenRetrievalException;

/**
 * @author dsilveira
 *
 */
public class AbstractWebState {

	private EnumWebStateName stateName;
	
	private WebService webService;
	private Post post;
	
	private int responseCode;
	private String error;
	private String token;
	

	public WebService getWebService() {
		return webService;
	}

	public void setWebService(WebService webService) {
		this.webService = webService;
	}
	
	public AbstractViewDevelopper getDevelopper(){
		return this.webService.getDevelopper();
	}
	
	public ViewApk getApk(){
		return this.webService.getApk();
	}

	public void setPost(Post post) {
		this.post= post;
	}
	
	public Post getPost() {
		return post;
	}	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}	
	
	public void rememberToken() throws UnsuccessfullTokenRetrievalException, JSONException, IdUndetectedBySoupException {
		webService.getDevelopper().setToken( getPost().getToken() );
	}
	
	
	protected void setStateName(EnumWebStateName stateName) {
		this.stateName = stateName;
	}


	protected void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}


	public void setError(String error) {
		this.error = error;
	}
	

	public EnumWebStateName getStateName() {
		return stateName;
	}


	public String getError() {
		return error;
	}

	
	public int getResponseCode() {
		return responseCode;
	}


	public void post() throws UnsuccessfullPostException, MalformedURLException, ProtocolException, IOException, JSONException {
		return;
	}
	
	@Override
	public String toString(){
		return stateName.toString();
	}

}
