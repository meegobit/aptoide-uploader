/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.util.webstates;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.json.JSONException;

import android.util.Log;

import pt.caixamagica.aptoide.uploader.util.Endpoints;
import pt.caixamagica.aptoide.uploader.util.EnumErrorStates;
import pt.caixamagica.aptoide.uploader.util.EnumWebStateName;
import pt.caixamagica.aptoide.uploader.util.Post;
import pt.caixamagica.aptoide.uploader.util.WebService;
import pt.caixamagica.aptoide.uploader.util.webstates.exceptions.UnsuccessfullSubmitException;

/**
 * @author dsilveira
 *
 */
public class Submitting extends AbstractWebState {

	private static final String ENDPOINT = Endpoints.getUploadWS();

		
	public Submitting(WebService webService) {
		setWebService(webService);
		setPost( new Post(ENDPOINT) );
		getPost().setJsonResponseMode();
		setStateName( EnumWebStateName.SUBMITTING );
		Log.d("AptoideUploader-WebState", getStateName().toString());
	}
	
	@Override
	public void post() throws UnsuccessfullSubmitException, MalformedURLException, ProtocolException, IOException, JSONException {
		getPost().buildSubmitBodyNoFooter( getDevelopper(), getApk() );
		setResponseCode( getPost().multipartOutput() );
		
		if(getResponseCode() != 200){
			getPost().hasErrors();
			setError( getPost().getErrors() );
			getWebService().setErrorName(EnumErrorStates.HTTP_SEQUENCE_ERROR);
			throw new UnsuccessfullSubmitException( getError() );
		}else if( getPost().hasErrors() ){
			setError( getPost().getErrors() );
			if(getError().equals("[\"The file you sent is missing. Maybe the form session has expired. Please upload the file again.\"]")){
				getWebService().setErrorName(EnumErrorStates.CONNECTION_ERROR);
			}
			if(getError().equals("[\"Missing token parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_TOKEN);
			}
			if(getError().equals("[\"Missing apk parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_APK);
			}
			if(getError().equals("[\"Missing apkname parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_APK_NAME);
			}
			if(getError().equals("[\"Missing description parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_DESCRIPTION);
			}
			if(getError().equals("[\"Missing rating parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_RATING);
			}
			if(getError().equals("[\"Missing category parameter\"]")){
				getWebService().setErrorName(EnumErrorStates.MISSING_CATEGORY);
			}
			if(getError().equals("[\"Invalid token!\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_TOKEN);
			}
			if(getError().equals("[\"Invalid repo!\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_REPO);
			}
			if(getError().equals("[\"You need to upload an APK\"]") || getError().equals("[\"An invalid APK was received. Please verify that you selected the right file, and try again.\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_APK);
			}
			if(getError().equals("[\"Invalid rating!\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_RATING);
			}
			if(getError().equals("[\"Invalid category!\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_CATEGORY);
			}
			if(getError().equals("[\"The website is not a valid URL.\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_WEBSITE);
			}
			if(getError().equals("[\"The e-mail address is not valid.\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_EMAIL);
			}
			if(getError().equals("[\"Token doesn't match with Repo.\"]")){
				getWebService().setErrorName(EnumErrorStates.TOKEN_INCONSISTENT_WITH_REPO);
			}
			if(getError().equals("[\"Unable to upload the apk. Please try again.\"]") || getError().equals("[\"The file transfer stopped before the upload was complete. Please try again.\"]") || getError().equals("[\"The file transfer failed for some unknown reason. Please try again.\"]")){
				getWebService().setErrorName(EnumErrorStates.BAD_APK_UPLOAD);
			}
			if(getError().equals("[\"The file(s) you uploaded exceeds the maximum allowed size.\"]")){
				getWebService().setErrorName(EnumErrorStates.APK_TOO_BIG);
			}
			if(getError().equals("[\"Due to Intelectual Property reasons, it's not possible to upload the required APK. If you are the developer\\/owner of the application, please contact Bazaar Staff.\"]")){
				getWebService().setErrorName(EnumErrorStates.BLACK_LISTED);
			}
		
			if (getWebService().getErrorName().equals(EnumErrorStates.NO_ERROR)){
				getWebService().setErrorName(EnumErrorStates.UNPREDICTABLE_ERROR);
			}
			throw new UnsuccessfullSubmitException( getError() );
		}
		getWebService().setCookie( getPost().getCookie() );
		
		getWebService().setWebState(new Submitted(getWebService()));

		return;
	}

}
