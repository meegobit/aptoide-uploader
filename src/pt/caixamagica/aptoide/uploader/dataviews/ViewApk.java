/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader.dataviews;

import java.io.Serializable;
import java.util.ArrayList;


public class ViewApk implements Serializable{

	private static final long serialVersionUID = 1773256785238541926L;
	
	private String path;
	private String name;
	
	private String description;
	private String repository;
	private String category;
	private String rating;
	
	private ArrayList<String> screenShotsPaths = new ArrayList<String>(5);
	
	
	public ArrayList<String> getScreenShotsPaths() {
		return screenShotsPaths;
	}

	public void setScreenShotsPaths(ArrayList<String> paths) {
		this.screenShotsPaths = paths;
	}
	
	public void addScreenShotPath(String path){
		screenShotsPaths.add(path);
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getPath() {
		return path;
	}

	public String getName() {
		return name;
	}


	public ViewApk(String name, String path){
		this.path = path;
		this.name = name;
	}
	
	@Override
	public String toString(){
		return "App name: \""+ name +"\" description: \""+ description 
		   +"\" category: \""+ category +"\" rating: \""+ rating +"\" path: \""+ path +"\""+"Repository: \""+ repository +"\"";
	}
	
}
