/*
 * Aptoide Uploader		uploads android apps to yout Bazaar repository
 * Copyright (C) 20011  Duarte Silveira
 * duarte.silveira@caixamagica.pt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package pt.caixamagica.aptoide.uploader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.caixamagica.aptoide.uploader.dataviews.*;
import pt.caixamagica.aptoide.uploader.util.*;
import pt.caixamagica.aptoide.uploader.util.webstates.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SimpleAdapter.ViewBinder;

public class AptoideUploader extends Activity {
	
	private Handler handler = new Handler();
	
	private WebService webService;
	private Persistence persistence;
	
	private AbstractViewDevelopper developper;
	private ArrayList<ViewApk> selectedApks;
	private ViewApk uploadingApk;
	private ArrayList<ViewApk> readyForUpload;
	private ArrayList<ViewApk> uploadedApks;
	private ArrayList<ViewApk> failedUploads;
		
	private LoginFormScreen loginForm;
	private SelectAppScreen selectAppScreen;
	@SuppressWarnings("unused")
	private SubmitFormScreen submitForm;
	@SuppressWarnings("unused")
	private UploadedAppsScreen uploadedAppsScreen;
	
	private EnumUIStateName uiState;
	
	private EnumErrorStates errorState;
	private boolean reLogin;
	private boolean requestDifferentRepoName;
	
	
	
	public EnumUIStateName getUiState() {
		return uiState;
	}

	public void setUiState(EnumUIStateName uiState) {
		this.uiState = uiState;
	}

	public EnumWebStateName getWebState(){
		return webService.getStateName();
	}

	public EnumErrorStates getErrorState() {
		return errorState;
	}

	public void setErrorState(EnumErrorStates errorState) {
		this.errorState = errorState;
	}
	

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        
        persistence = new Persistence(getApplicationContext());
    	webService = new WebService();
    	
    	developper = new NullViewDevelopper();
    	
    	errorState = EnumErrorStates.NO_ERROR;
        
    	presentSplash();
    	
    }
	
	public void presentSplash(){
		setContentView(R.layout.splash_screen);
		handler.postDelayed(new Runnable() {
            public void run() {
            	presentLoginForm(false);
            }
        }, 3000);
	}
   

    public void presentLoginForm(boolean force){
    	if(force){
    		loginForm = new LoginFormScreen(force);
    	}else{
    		loginForm = new LoginFormScreen();
    	}
    	
    }
 
    
    protected void login(){
    	webService.setWebState(new LoggingIn(webService));
    	
    	developper = new ViewDevelopper( loginForm.getUsername(), loginForm.getPassword() );
    	developper.setRepository(loginForm.getRepository());
    	developper.setBatchUploader( loginForm.isBatchUploader() );
    	new ShowProgressDialog().execute(new ViewApk(null,null));
    	
    }  
    
    
    public void presentInstalledApps(){
    	
    	selectAppScreen = new SelectAppScreen(this);
    }
  
    
    public void selectApps(){

    	ArrayList<ViewLocalApp> localApps = selectAppScreen.apps;
    	selectedApks = new ArrayList<ViewApk>();
    	uploadedApks = new ArrayList<ViewApk>();
    	
    	readyForUpload = new ArrayList<ViewApk>();
    	failedUploads = new ArrayList<ViewApk>();
    	
    	for (ViewLocalApp localApp : localApps) { 		
    		if(localApp.isSelected()){
    			selectedApks.add(new ViewApk( localApp.getName() , localApp.getApkPath() ));
    		}
		}
		
		presentSubmitForms();
    }
    
    
    public void presentSubmitForms(){
	    	if(selectedApks.size()>0){
	    		uploadingApk = selectedApks.remove(0);
	    		submitForm = new SubmitFormScreen(this);
	    	}else{
	    		if(developper.isBatchUploader()){
	    			batchSubmit();
	    		}else{
	    			presentSubmitted();
	    		}
	    	}
		
    }
    
    
    protected void submit(){
    	if(developper.isBatchUploader()){
    		readyForUpload.add(uploadingApk);
    		presentSubmitForms();
    	}else{
	    	webService.setWebState(new Submitting(webService));
	
			new ShowProgressDialog().execute(uploadingApk);
		}
    }
    
    protected void batchSubmit(){
    	setContentView(R.layout.batch_upload_status);
    	if(readyForUpload.size()>0){
    		uploadingApk = readyForUpload.remove(0);
    		webService.setWebState(new Submitting(webService));
    		
			new ShowProgressDialog().execute(uploadingApk);
    	}else{
    		presentSubmitted();
    	}
    }
    
    public void presentSubmitted(){
    	uploadedAppsScreen = new UploadedAppsScreen(this, uploadedApks, failedUploads);
    }

    
    
    public void clearState(){
    	uploadedApks.clear();
    	uploadingApk = null;
    	selectedApks.clear();
    	failedUploads.clear();
    	readyForUpload.clear();

    	errorState = EnumErrorStates.NO_ERROR;
    	
    	webService = new WebService();
    	webService.setWebState(new LoggedIn(webService));
    }
    
    public void restart(){
    	clearState();
    	presentInstalledApps();
    }
    
    
    
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_options, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			presentLoginForm(true);
			break;
		case R.id.about:
			showAbout();
			break;
		case R.id.exit:
			exit();
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
    @Override
	public void finish() {
    	switch (getUiState()) {
		case PRESENTING_LOG_IN_FORM:
			if(loginForm.isInitialScreen){
				return;
			}else{
				presentInstalledApps();
			}
			break;

		case PRESENTING_UPLOADS_REPORT:
			presentInstalledApps();
			break;
			
		default:
			break;
		}
    }

	protected int exit(){
    	super.finish();
    	
    	return 0;
    }
    
    
    
    
    
    public void showAbout(){
    	AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
    	alertBuilder.setCancelable(false)
//    				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//    					public void onClick(DialogInterface dialog, int id) {
//    						// Action for 'Yes' Button
//    					}
//    				})    	
    				.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
    					public void onClick(DialogInterface dialog, int id) {
    						dialog.cancel();
    					}
    				})
    				.setMessage(R.string.about)
    				;
    	
    	AlertDialog alert = alertBuilder.create();
    	
    	alert.setTitle(R.string.about_title);
    	alert.setIcon(R.drawable.icon);
    	
    	alert.show();
    }
    
	
	
	
	
	
	private class ShowProgressDialog extends AsyncTask<ViewApk, Void, Integer>{
		private final ProgressDialog dialog = new ProgressDialog(AptoideUploader.this);
		
		PowerManager pm;
		WakeLock keepScreenOn;
		
		@Override
		protected void onPreExecute() {
			pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			if(getWebState() == EnumWebStateName.LOGGING_IN){
				this.dialog.setMessage("Logging In...");
			}else if(getWebState() == EnumWebStateName.SUBMITTING){
				this.dialog.setMessage("Uploading Apk...");
			}
			
			this.dialog.show();
			super.onPreExecute();
		}
		
		@Override
		protected Integer doInBackground(ViewApk... apk) {
			keepScreenOn = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "My Tag");
			keepScreenOn.acquire();

			if(getWebState() == EnumWebStateName.LOGGING_IN){
		    	webService.login(developper);
			}else if(getWebState() == EnumWebStateName.SUBMITTING){
				webService.submit(developper, apk[0]);
			}
			return 1;
		}

		@Override
		protected void onPostExecute(Integer result) {
			keepScreenOn.release();
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			
			validateWebServicePosts();
			
			super.onPostExecute(result);
		}
		 		
	}
	
	
	
	
	    
    public void validateWebServicePosts(){
    	if( getWebState() == EnumWebStateName.ERROR){
    		errorState = webService.getErrorName();
    		if(developper.isBatchUploader()){
    			Toast.makeText(getApplicationContext(), uploadingApk.getName()+" failed to uploaded", Toast.LENGTH_LONG).show();
    			Log.d("AptoideUploader-submit", "failed to uploaded: "+uploadingApk.toString());
    			failedUploads.add(uploadingApk);
    			
    			batchSubmit();
    		}else{
	    		switch (errorState) {
	    			case CONNECTION_ERROR:
	    				ConnectivityManager netstate = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    				boolean connectionAvailable = false;
	    				try {
	    					connectionAvailable = netstate.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED;
	    					connectionAvailable = connectionAvailable || netstate.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED;
	    					connectionAvailable = connectionAvailable || netstate.getNetworkInfo(6).getState() == NetworkInfo.State.CONNECTED;
	    				} catch (Exception e) { }
	    				try {
	    					connectionAvailable = connectionAvailable || netstate.getNetworkInfo(9).getState() == NetworkInfo.State.CONNECTED;
	    				} catch (Exception e) { }
	    				if(connectionAvailable){
	    					Toast.makeText(getApplicationContext(), R.string.failed_server_connection, Toast.LENGTH_LONG).show();
	    				}else{
	    					Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();	
	    				}
	    				break;
	    			case BAD_LOGIN:
	    	    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
			    		presentLoginForm(true);
						break;
					case MISSING_TOKEN:
					case BAD_TOKEN:
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						if(reLogin){
							reLogin = false;
							errorState = EnumErrorStates.NO_ERROR;
							
							webService.setWebState(new Submitting(webService));
							new ShowProgressDialog().execute(uploadingApk);
						}else{
							reLogin = true;
							
							webService.setWebState(new LoggingIn(webService));
							new ShowProgressDialog().execute(new ViewApk(null,null));
						}
						break;
					case BAD_REPO:
					case TOKEN_INCONSISTENT_WITH_REPO:
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						if(requestDifferentRepoName){
							requestDifferentRepoName = false;
							errorState = EnumErrorStates.NO_ERROR;
							
							webService.setWebState(new Submitting(webService));
							new ShowProgressDialog().execute(uploadingApk);
						}else{
							requestDifferentRepoName = true;
							
							presentLoginForm(true);
						}
						break;
					case MISSING_APK:
					case BAD_APK:
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						Log.d("AptoideUploader-Validate", errorState.toString());
						webService.setWebState(new Submitting(webService));
						new ShowProgressDialog().execute(uploadingApk);
						break;
					case MISSING_DESCRIPTION:
					case BAD_CATEGORY:
					case BAD_WEBSITE:
					case BAD_EMAIL:
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						submitForm = new SubmitFormScreen(this, true);
						break;
					case BAD_APK_UPLOAD:
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						new ShowProgressDialog().execute(uploadingApk);
						break;
						
					case BLACK_LISTED:						
					default:
						Log.d("AptoideUploader-Validate", errorState.toString());
			    		Toast.makeText(getApplicationContext(), webService.getError(), Toast.LENGTH_LONG).show();
						restart();
						break;
				}
    			return;
    		}
    	}else if(getWebState() == EnumWebStateName.LOGGED_IN){
			persistence.open();
			persistence.storeDefaultLoginData(developper);
			persistence.close();
	    	
	    	Toast.makeText(getApplicationContext(), "Successfully logged in!", Toast.LENGTH_LONG).show();
			Log.d("AptoideUploader-login", "successfully logged in: "+developper.toString());

	    	if(reLogin){
				reLogin = false;
				errorState = EnumErrorStates.NO_ERROR;
				
				webService.setWebState(new Submitting(webService));
				new ShowProgressDialog().execute(uploadingApk);
				return;
			}else if(requestDifferentRepoName){
				requestDifferentRepoName = false;
				errorState = EnumErrorStates.NO_ERROR;
				
				webService.setWebState(new Submitting(webService));
				new ShowProgressDialog().execute(uploadingApk);
				return;
			}else{
				presentInstalledApps();
			}
		}else if(getWebState() == EnumWebStateName.SUBMITTED){
			Toast.makeText(getApplicationContext(), uploadingApk.getName()+" successfully uploaded", Toast.LENGTH_LONG).show();
			Log.d("AptoideUploader-submit", "successfully uploaded: "+uploadingApk.toString());
			uploadedApks.add(uploadingApk);
			
			if(developper.isBatchUploader()){
				batchSubmit();
			}else{
				presentSubmitForms();
			}
			
		}
    	errorState = EnumErrorStates.NO_ERROR;
    }
	
	
	
	
	
	public class LoginFormScreen {

		private TextView errorIntro;
		private EditText username;
		private EditText password;
		private EditText repository;
		private CheckBox batch_uploader;
		private Button exitButton;
		private Button loginButton;
		private TextView registerLink;
		
		private boolean isInitialScreen;
		
		public String getUsername() {
			return username.getText().toString();
		}
		
		public String getPassword() {
			return password.getText().toString();
		}
		
		public String getRepository() {
			return repository.getText().toString();
		}
		
		public boolean isBatchUploader() {
			return batch_uploader.isChecked();
		}
		
		public LoginFormScreen(boolean force) {
			setUiState(EnumUIStateName.PRESENTING_LOG_IN_FORM);
			
			persistence.open();
	    	if( persistence.defaultExists() ){
	    		developper = persistence.fetchDefaultLogin();
	    		Log.d("AptoideUploader-login", "retrieved developper: "+developper.toString());
	    		isInitialScreen = false;
	    	}else{
	    		isInitialScreen = true;
	    	}
	    	persistence.close();
	    	
	    	if(!force && !developper.isNull()){
	    		Toast.makeText(getApplicationContext(), "Logged in as: "+developper.getUsername(), Toast.LENGTH_LONG).show();
	    		presentInstalledApps();
	    		webService.setWebState(new LoggedIn(webService));
	    		return;
    		}
	
			webService.setWebState(new LoggingIn(webService));
			
	    	setContentView(R.layout.login_form);
	    	if(errorState.equals(EnumErrorStates.BAD_LOGIN) || errorState.equals(EnumErrorStates.TOKEN_INCONSISTENT_WITH_REPO) || errorState.equals(EnumErrorStates.BAD_REPO)){
	    		setErrorIntro();
	    	}
			setUsernameBox();
			setPasswordBox();
			setRepositoryBox();
			setBatchUploadCheckBox();
			setExitButton();
			setLoginButton();
			setRegisterLink();
		}
		
		public LoginFormScreen(){
			 this(false);
		}
		
		
		public void setErrorIntro(){
			errorIntro = (TextView) findViewById(R.id.login_intro);
			errorIntro.setTextColor(Color.RED);
			switch (errorState) {
			case BAD_LOGIN:
				errorIntro.setText(R.string.bad_login);
				break;
			case BAD_REPO:
				errorIntro.setText(R.string.bad_repo);
				break;
			case TOKEN_INCONSISTENT_WITH_REPO:
				errorIntro.setText(R.string.token_inconsistent_with_repo);
				break;
			default:
				break;
			}
		}
		
		
		public void setUsernameBox(){
			username = (EditText) findViewById(R.id.username);
			if(!developper.isNull()){
				username.setText(developper.getUsername());
			}
			username.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-login", "Username: " + username.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setPasswordBox(){
			password = (EditText) findViewById(R.id.password);
			if(!developper.isNull()){
				password.setText(developper.getPassword());
			}
			password.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-login", "Password: " + password.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setRepositoryBox(){
			repository = (EditText) findViewById(R.id.repository);
			if(!developper.isNull()){
				repository.setText(developper.getRepository());
			}
			repository.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-login", "Repository: " + repository.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setBatchUploadCheckBox(){
			batch_uploader = (CheckBox) findViewById(R.id.batch_upload);
			if( isInitialScreen ){
				batch_uploader.setVisibility(View.GONE);
			}else{
				batch_uploader.setVisibility(View.VISIBLE);
			}
			
			if(!developper.isNull()){
				batch_uploader.setChecked(developper.isBatchUploader());
			}
			batch_uploader.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d("AptoideUploader-login", "Batch upload: " + batch_uploader.isChecked());
				}
			});
		}
		
		public void setExitButton(){
			exitButton = (Button) findViewById(R.id.exit);
			exitButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!isInitialScreen){
						presentInstalledApps();
					}else{
						exit();
					}
				}
			  });
		}
		
		public void setLoginButton(){
			loginButton = (Button) findViewById(R.id.login);
			loginButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(loginButton.getWindowToken(), 0);

					login();				
				}
			  });
		}
		
		public void setRegisterLink(){
			registerLink = (TextView) findViewById(R.id.register_link);
			registerLink.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Uri uri = Uri.parse("http://www.bazaarandroid.com/account/new-user?forcemobilepage=0");
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(intent); 
				}
			  });
		}
		
	}
	
	
    
    
    
	public class SelectAppScreen {
		
		Context context;
		SimpleAdapter appsIconsAdapter;
		
		private GridView appsScreen;
		private PackageManager packageManager;
		private ArrayList<ViewLocalApp> apps;
		private List<Map<String, Object>> appsIcons;
		
		private Button exitButton;
		private Button continueButton;
		
		class gridBinder implements ViewBinder
		{
			public boolean setViewValue(View view, Object data, String textRepresentation)
			{
				if(view.getClass().toString().equalsIgnoreCase("class android.widget.TextView")){
					TextView textView = (TextView)view;
					textView.setText(textRepresentation);
				}else if(view.getClass().toString().equalsIgnoreCase("class android.widget.ImageView")){
					ImageView imageView = (ImageView)view;	
					
					imageView.setImageBitmap((Bitmap)data);
				}else{
					return false;
				}
				return true;
			}
		}
		

		protected void fillApps(){
			final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			packageManager.queryIntentActivities( mainIntent, 0);
			List<ResolveInfo> appsInfo = packageManager.queryIntentActivities( mainIntent, 0);
			apps = new ArrayList<ViewLocalApp>();
			appsIcons = new ArrayList<Map<String, Object>>();
		
			for (ResolveInfo resolveInfo : appsInfo) {
				String apkPath = resolveInfo.activityInfo.applicationInfo.sourceDir;
				File apk = new File(apkPath);
				
				if(!apkPath.split("[/]+")[1].equals("system")  &&  apk.length() < 20000000 ){
				ViewLocalApp v = new ViewLocalApp(resolveInfo, packageManager);
				apps.add(v);
				Log.d("AptoideUploader-package", v.getName()+" > "+v.getApkPath());
				
				HashMap<String, Object> app = new HashMap<String, Object>();
				app.put("name", v.getName());
				app.put("icon", ((BitmapDrawable)v.getIcon()).getBitmap());
				appsIcons.add(app);
				}
			}
		
		}
		
		public void markSelected(int position) {
			Bitmap bmp = ((Bitmap)appsIcons.get(position).get("icon")).copy(Bitmap.Config.ARGB_8888, true);
		    Canvas canvas = new Canvas(bmp);
		    Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
		    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.check), 0, 0, paint);
		    appsIcons.get(position).put("icon", bmp);
		    appsIconsAdapter.notifyDataSetChanged();
		    
		}
		
		public void markUnselected(int position){
			appsIcons.get(position).put("icon", ((BitmapDrawable)apps.get(position).getIcon()).getBitmap());
		    appsIconsAdapter.notifyDataSetChanged();
		}

		public SelectAppScreen(Context context){
	    	this.context = context;
	    	
			setUiState(EnumUIStateName.PRESENTING_APPS_SELECTOR);
	    	
	    	setContentView(R.layout.app_selector);
	    	setExitButton();
			setContinueButton();
			
			packageManager = context.getPackageManager();
			fillApps();
			
			appsScreen = (GridView) findViewById(R.id.grid);
			appsIconsAdapter = new SimpleAdapter(context, appsIcons, R.layout.icon,
					new String[] {"icon", "name"}, new int[]{ R.id.icon_image, R.id.icon_text});
			appsIconsAdapter.setViewBinder(new gridBinder());
			
			appsScreen.setAdapter(appsIconsAdapter);

			appsScreen.setOnItemClickListener(new OnItemClickListener() {
		
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view,
						int position, long positionLong) {
					
					if(!apps.get(position).isSelected()){
						apps.get(position).setSelected(true);
						markSelected(position);
					}else{
						apps.get(position).setSelected(false);
						markUnselected(position);
					}

					Log.d("AptoideUploader-grid","position ["+position+"], name ["+apps.get(position).getName()+"], apk ["+apps.get(position).getApkPath()+"], selected: "+apps.get(position).isSelected());

					
				}
			});
			
		}
		
		
		public void setExitButton() {
			exitButton = (Button) findViewById(R.id.select_exit);
			exitButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					exit();			
				}
			  });
		}
		
		public void setContinueButton() {
			continueButton = (Button) findViewById(R.id.select_continue);
			continueButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(continueButton.getWindowToken(), 0);
					
					selectApps();
				}
			  });
		}

	}
	
	
	
	
	
	public class SubmitFormScreen {
		
		private ScrollView scrollView;
		
		private TextView errorIntro;
		
		private EditText appName;
		private Spinner appCategory;
		private Spinner appRating;
		
//		private TextView apkPath;

		private EditText appDescription;
		private EditText phoneNumber;
		private EditText eMail;
		private EditText webURL;
		private Button backButton;
		private Button submitButton;
		
		public String getAppName() {
			return appName.getText().toString();
		}
		
		public String getAppDescription() {
			return appDescription.getText().toString();
		}
		
		public String getAppCategory() {
			return appCategory.getSelectedItem().toString();
		}
		
		public String getAppCategoryPosition() {
			return Integer.toString( appCategory.getSelectedItemPosition()+1 );
		}
		
		public String getAppRating() {
			return appRating.getSelectedItem().toString();
		}
		
		public String getAppRatingPosition() {
			return Integer.toString( appRating.getSelectedItemPosition()+1 );
		}
		
		public String getPhoneNumber() {
			return phoneNumber.getText().toString();
		}
		
		public String getEMail() {
			return eMail.getText().toString();
		}
		
		public String getWebURL() {
			return webURL.getText().toString();
		}
		
		public SubmitFormScreen(Context context){
			setUiState(EnumUIStateName.PRESENTING_SUBMIT_FORM);
			
	    	setContentView(R.layout.submit_form);
			setAppNameBox();
			setAppRatingBox(context);
//			setApkPath();
			setAppCategoryBox(context);
			setAppDescriptionBox();
			setPhoneNumberBox();
			setEMailBox();
			setWebURLBox();
			setBackButton();
			setSubmitButton();
		}
		
		public SubmitFormScreen(Context context, boolean error){
			if(error == true){
				switch (errorState) {
				case MISSING_DESCRIPTION:
					setErrorIntro(false, false, true, false, false);					
					break;
				case BAD_CATEGORY:
					setErrorIntro(false, true, false, false, false);
					break;
				case BAD_WEBSITE:
					setErrorIntro(false, false, false, true, false);
					break;
				case BAD_EMAIL:
					setErrorIntro(false, false, false, false, true);
					break;

				default:
					break;
				}
			}
			
		}
		
		private void prepareErrorIntro(){
			errorIntro = (TextView) findViewById(R.id.form_intro);
			errorIntro.setTextColor(Color.RED);
			
			scrollView = (ScrollView) findViewById(R.id.scroll_view);
			scrollView.pageScroll(View.FOCUS_UP);
		}
		
		
		public void setErrorIntro(boolean missingAppName, boolean missingAppCategory, boolean missingDescription, boolean badWebsite, boolean badEmail){

			prepareErrorIntro();
			
			if(missingAppName && missingAppCategory && missingDescription){
				errorIntro.setText(R.string.missing_name_and_category_and_description);
			}else if(missingAppName && missingAppCategory && badWebsite){
				errorIntro.setText(R.string.missing_name_and_category_and_bad_website);
			}else if(missingAppName && missingAppCategory && badEmail){
				errorIntro.setText(R.string.missing_name_and_category_and_bad_email);
			}else if(missingAppName && missingAppCategory){
				errorIntro.setText(R.string.missing_name_and_category);
			}else if(missingAppName && missingDescription){
				errorIntro.setText(R.string.missing_name_and_description);
			}else if(missingAppName && badWebsite){
				errorIntro.setText(R.string.missing_name_and_bad_website);
			}else if(missingAppName && badEmail){
				errorIntro.setText(R.string.missing_name_and_bad_email);
			}else if(missingAppCategory && missingDescription){
				errorIntro.setText(R.string.missing_category_and_description);
			}else if(missingAppCategory && badWebsite){
				errorIntro.setText(R.string.missing_category_and_bad_website);
			}else if(missingAppCategory && badEmail){
				errorIntro.setText(R.string.missing_category_and_bad_email);
			}else if(missingAppName){
				errorIntro.setText(R.string.missing_apk_name);
			}else if(missingAppCategory){
				errorIntro.setText(R.string.missing_category);
			}else if(missingDescription){
				errorIntro.setText(R.string.missing_description);
			}else if(badWebsite){
				errorIntro.setText(R.string.bad_website);
			}else if(badEmail){
				errorIntro.setText(R.string.bad_email);
			}
			
			scrollView.pageScroll(View.FOCUS_UP);
		}
		
		
		public void setAppNameBox() {
			appName = (EditText) findViewById(R.id.form_name);
			appName.setText(uploadingApk.getName());
			appName.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-form", "App name: " + appName.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setAppRatingBox(Context context) {
			appRating = (Spinner)findViewById(R.id.form_rating);
			ArrayAdapter<CharSequence> newRatingAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.rating_array, android.R.layout.simple_spinner_item);
			newRatingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			appRating.setAdapter(newRatingAdapter);
			if(uploadingApk.getRating() !=  null){
				appRating.setSelection(Integer.parseInt(uploadingApk.getRating())-1);
			}
			appRating.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					Log.d("AptoideUploader-form", "App rating: " + (appRating.getSelectedItemPosition()+1) + " - " + appRating.getSelectedItem());
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {				
				} 
			});
		}
		
		
		
		public void setAppCategoryBox(Context context) {
			String categoryPrompt[] = {"Optional App category"};
			appCategory = (Spinner)findViewById(R.id.form_category);
			ArrayAdapter<CharSequence> categoryAdapter = new ArrayAdapter<CharSequence>(context, android.R.layout.simple_spinner_item, categoryPrompt);
			categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			appCategory.setAdapter(categoryAdapter);
			if(uploadingApk.getCategory() !=  null){
				appCategory.setSelection(Integer.parseInt(uploadingApk.getCategory())-1);
			}
			appCategory.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					ArrayAdapter<CharSequence> newCategoryAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.category_array, android.R.layout.simple_spinner_item);
					newCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				    appCategory.setAdapter(newCategoryAdapter);
				    appCategory.setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
							Log.d("AptoideUploader-form", "App category: " + (appCategory.getSelectedItemPosition()+1) + " - " + appCategory.getSelectedItem());
						}
						@Override
						public void onNothingSelected(AdapterView<?> arg0) {				
						} 
					});
				    return false;
				}
			});
		}
		
		public void setAppDescriptionBox() {
			appDescription = (EditText) findViewById(R.id.form_desc);
			if(uploadingApk.getDescription() !=  null){
				appDescription.setText(uploadingApk.getDescription());
			}
			appDescription.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-form", "App description: " + appDescription.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setPhoneNumberBox() {
			phoneNumber = (EditText) findViewById(R.id.form_phone);
			if(developper.getPhone() != null){
				phoneNumber.setText(developper.getPhone());
			}
			phoneNumber.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-form", "Phone Number: " + phoneNumber.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setEMailBox() {
			eMail = (EditText) findViewById(R.id.form_e_mail);
			if(developper.getEmail() != null){
				eMail.setText(developper.getEmail());
			}
			eMail.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-form", "E-mail: " + eMail.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setWebURLBox() {
			webURL = (EditText) findViewById(R.id.form_url);
			if(developper.getWebURL() != null){
				webURL.setText(developper.getWebURL());
			}
			webURL.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	Log.d("AptoideUploader-form", "Web url: " + webURL.getText());
			          return true;
			        }
					return false;
				}
			});
		}
		
		public void setBackButton() {
			backButton = (Button) findViewById(R.id.form_back);
			backButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					presentInstalledApps();			
				}
			  });
		}
		
		public void setSubmitButton() {
			submitButton = (Button) findViewById(R.id.form_submit);
			submitButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(submitButton.getWindowToken(), 0);
					
					
					if(appName.getText().toString().equals("") && appCategory.getSelectedItem().equals("Optional App category") && (getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.MISSING_DESCRIPTION) && appDescription.getText().toString().equals(""))){
//						setErrorIntro(true, true, true, false, false);
//						Log.d("AptoideUploader-submit-error", "no app name   ,   no category   and   no description"); 
					}else if(appName.getText().toString().equals("") && (getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.BAD_CATEGORY) && appCategory.getSelectedItem().equals("App category"))){
						setErrorIntro(true, true, false, false, false);
						Log.d("AptoideUploader-submit-error", "no app name   and   no category"); 
					}else if(appName.getText().toString().equals("") && (getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.MISSING_DESCRIPTION) && appDescription.getText().toString().equals(""))){
						setErrorIntro(true, false, true, false, false);
						Log.d("AptoideUploader-submit-error", "no app name   and   no description"); 
					}else if((getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.BAD_CATEGORY) && appCategory.getSelectedItem().equals("Optional App category")) && (getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.MISSING_DESCRIPTION) && appDescription.getText().toString().equals(""))){
						setErrorIntro(false, true, true, false, false);
						Log.d("AptoideUploader-submit-error", "no category   and   no description"); 
					}else if(appName.getText().toString().equals("")){
						setErrorIntro(true, false, false, false, false);
						Log.d("AptoideUploader-submit-error", "no app name"); 
					}else if( getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.BAD_CATEGORY) && appCategory.getSelectedItem().equals("Optional App category") ){
						setErrorIntro(false, true, false, false, false);
						Log.d("AptoideUploader-submit-error", "no category"); 
					}else if( getWebState().equals(EnumWebStateName.ERROR) && errorState.equals(EnumErrorStates.MISSING_DESCRIPTION) && appDescription.getText().toString().equals("") ){
						setErrorIntro(false, false, true, false, false);
						Log.d("AptoideUploader-submit-error", "no description"); 
					}else{
						Log.d("AptoideUploader-submit", "about to submit");
						
						uploadingApk.setDescription(getAppDescription());
						if(!appCategory.getSelectedItem().equals("Optional App category")){
							uploadingApk.setCategory( getAppCategoryPosition() );
						}
						uploadingApk.setRating( getAppRatingPosition() );
						Log.d("AptoideUploader-apk", uploadingApk.toString());
						
						if(!getPhoneNumber().equals("")){
							developper.setPhone(getPhoneNumber());
						}else{
							developper.setPhone(null);
						}
						
						if(!getEMail().equals("")){
							developper.setEmail(getEMail());
						}else{
							developper.setEmail(null);
						}
						
						if(!getWebURL().equals("")){
							developper.setWebURL(getWebURL());
						}else{
							developper.setWebURL(null);
						}
						
						Log.d("AptoideUploader-dev", developper.toString());
						
						submit();
					}
				}
			  });
		}
		
//		public void setApkPath() {
//			apkPath = (TextView) findViewById(R.id.form_apk_path);
//		    apkPath.setText(uploadingApk.getPath());
//		}
		
	}
	
	
	
public class UploadedAppsScreen {
		
		Context context;
		
		private ListView uploadedListView;
		private ListView notUploadedListView;
		private ArrayList<Map<String, String>> uploadedList;
		private ArrayList<Map<String, String>> notUploadedList;
		
		private Button exitButton;
		

		protected void fillApps(ArrayList<ViewApk> uploadedApks){
			uploadedList = new ArrayList<Map<String, String>>();
		
			for (ViewApk uploadedApk : uploadedApks) {
				Log.d("AptoideUploader-uploaded", uploadedApk.getName()+" > "+uploadedApk.getPath());
				
				HashMap<String, String> app = new HashMap<String, String>();
				app.put("name", uploadedApk.getName());
				app.put("path", uploadedApk.getPath());
				uploadedList.add(app);
			}
		
		}
		
		protected void fillFailedApps(ArrayList<ViewApk> failedUploads){
			notUploadedList = new ArrayList<Map<String, String>>();
		
			for (ViewApk notUploadedApk : failedUploads) {
				Log.d("AptoideUploader-not-uploaded", notUploadedApk.getName()+" > "+notUploadedApk.getPath());
				
				HashMap<String, String> app = new HashMap<String, String>();
				app.put("name", notUploadedApk.getName());
				app.put("path", notUploadedApk.getPath());
				notUploadedList.add(app);
			}
		
		}

		public UploadedAppsScreen(Context context, ArrayList<ViewApk> uploadedApks, ArrayList<ViewApk> failedUploads){
	    	this.context = context;
	    	
	    	setUiState(EnumUIStateName.PRESENTING_UPLOADS_REPORT);
	    	
	    	if(developper.isBatchUploader()){
	    		setContentView(R.layout.batch_upload_status);
	    	}else{
	    		setContentView(R.layout.upload_status);
	    	}
	    	setExitButton();
			
			fillApps(uploadedApks);
			
			uploadedListView = (ListView) findViewById(R.id.list);
			SimpleAdapter appsIconsAdapter = new SimpleAdapter(context, uploadedList, R.layout.one_line_row,
					new String[] {"name"}, new int[]{ R.id.uploaded_name});
			uploadedListView.setAdapter(appsIconsAdapter);
			

			if(developper.isBatchUploader() && !failedUploads.isEmpty()){
				fillFailedApps(failedUploads);

				notUploadedListView = (ListView) findViewById(R.id.failed_list);
				SimpleAdapter failedAppsIconsAdapter = new SimpleAdapter(context, notUploadedList, R.layout.one_line_row,
						new String[] {"name"}, new int[]{ R.id.uploaded_name});
				notUploadedListView.setAdapter(failedAppsIconsAdapter);		
			}
		}
		
		
		public void setExitButton() {
			exitButton = (Button) findViewById(R.id.uploaded_exit);
			exitButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					restart();			
				}
			  });
		}

	}

}